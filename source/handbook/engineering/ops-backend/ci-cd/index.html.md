---
layout: markdown_page
title: "CI/CD Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### CI/CD Team
{: #cicd}

The CI/CD Team is focused on all the functionality with respect to
Continuous Integration and Deployments.

This team maps to [Verify, Package, Release, and Configure](/handbook/product/categories/#ops).
