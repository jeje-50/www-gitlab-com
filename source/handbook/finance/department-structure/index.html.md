--- 
layout: markdown_page
title: "Department Structure"
---

## Department Structure

Below is a table showing the structure of GitLab departments as they exist in NetSuite. Please [check out the Team Page](/team/chart) to see our org chart.

| Sales | Cost of Sales | Marketing | Development | General & Administrative | GitLab.com | GitHost |
| :-----------: | :------: | :------------: | :-----------: | :------: | :------------: | :------------: |
| Business Development | Customer Solutions | Corporate Marketing | Meltano | PeopleOps | 
| Field Sales | Customer Support  | Field Marketing | Infrastructure | Finance  ||
|Account Management | |Growth Marketing | Product Management  | 
| Customer Success || Inbound Demand Generation  | 
| Indirect Channel || Outbound Demand Generation |||||
|Sales Management|| Outreach |||||
||| Product Marketing |||||

