---
layout: markdown_page
title: "Committer Program"
---

## On this page
{:.no_toc}

- TOC
{:toc}


### Plan

1. Recognize new code contributors after their first merged MR
1. Regular live-stream calls with the [Core Team](https://about.gitlab.com/core-team/)
1. Blog post series featuring contributors 
1. Pairing new code contributors with mentors

### Outreach Email Templates


