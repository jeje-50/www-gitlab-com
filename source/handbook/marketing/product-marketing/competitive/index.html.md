---
layout: markdown_page
title: "Competitive Intelligence"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What is competitive intelligence?
A function of [product marketing](/handbook/marketing/product-marketing) is to perform market analysis and gather information about competitors to better position GitLab in our go-to-market efforts.

## Market analysis
* [Application Security](application-security/)

## Competitor list
Here is a list of products that have offerings in various [product categories](/handbook/product/categories/). A user would need to patch together multiple solutions from this list in order to get all the functionality that is built-in to GitLab as a [single application for end-to-end DevOps](https://about.gitlab.com/). Other places that list tools in various DevOps categories include [CA](https://assessment-tools.ca.com/tools/continuous-delivery-tools/en?embed), [XebiaLabs](https://xebialabs.com/periodic-table-of-devops-tools/), and [CNCF](https://landscape.cncf.io/)

### Project management
#### Issue Tracking
- [Atlassian Jira](https://www.atlassian.com/software/jira)
- [Asana](https://asana.com/)
- [Microsoft VSTS](https://visualstudio.microsoft.com/team-services/)
- [Microsoft TFS](https://visualstudio.microsoft.com/tfs/)
- [Pivotal Tracker](https://www.pivotaltracker.com/)
- Bugzilla
- JetBrains
- Zoho Corporation
- Airbrake
- Axosoft
- Bugsnag
- Countersoft
- [Fog Creek Software](http://www.fogcreek.com/)
- Inflectra Corporation
- Redmine
- [BMC Remedy](http://www.bmc.com/it-solutions/remedy-itsm.html)
- QA Symphony
- FogBugz

#### Issue Boards
- [Atlassian Jira](https://www.atlassian.com/software/jira)
- [Asana](https://asana.com/)
- [Trello](https://trello.com/)
- [CollabNet VersionOne](https://www.collab.net/)
- Basecamp
- Wrike
- Podio
- Zoho Projects
- Freedcamps
- Teamwork Projects
- TrackVia
- Dapulse

### Portfolio Management
- [CollabNet VersionOne](https://www.collab.net/)
- [Atlassian Jira Portfolio](https://www.atlassian.com/software/jira/portfolio)
- [CA Technologies / Rally](https://www.ca.com/us.html)
- Wrike
- Plutora
- IBM Rational Team Concert
- HP Application Lifecycle Management

### Service desk
- [Atlassian Jira Service Desk](https://www.atlassian.com/software/jira/service-desk)
- [Zendesk](https://www.zendesk.com/)
- [Freshdesk](https://freshdesk.com/)
- [CA Service Desk Manager](https://www.ca.com/us/products/ca-service-desk-manager.html)

### Source code management
- [GitHub](https://github.com/)
- [Atlassian BitBucket](https://www.atlassian.com/software/bitbucket/features)
- [Microsoft VSTS](https://visualstudio.microsoft.com/team-services/)
- [Microsoft TFS](https://visualstudio.microsoft.com/tfs/)
- [CA Technologies / Rally](https://www.ca.com/us.html)
- IBM Rational Clear case
- Perforce
- Helix VCS
- Assembla
- Beanstalk
- [Rhodecode](https://rhodecode.com/features/community-vs-enterprise)
- [Mercurial](https://www.mercurial-scm.org/)
- [Subversion](https://subversion.apache.org/) aka SVN
- [Kallithea](https://kallithea-scm.org/)

### Code Review
- [Atlassian Crucible](https://www.atlassian.com/software/crucible)
- [Gerrit](https://www.gerritcodereview.com/)
- [Smart Bear Collaborator](https://smartbear.com/product/collaborator/overview/)
- [Phabricator](https://www.phacility.com/phabricator/)
- Codebrag
- Codestriker
- [Rhodecode](https://rhodecode.com/features/community-vs-enterprise)
- Review Assistant
- Review Board
- Code Climate

### Web IDE
- [AWS Cloud9](https://aws.amazon.com/cloud9/)
- [Code Anywhere](https://codeanywhere.com/)
- [Koding](https://www.koding.com/)
- [repl.it](https://repl.it/)
- [Codechef IDE](https://www.codechef.com/ide)

### Wiki
- [Atlassian Confluence](https://www.atlassian.com/software/confluence)
- [SharePoint](https://products.office.com/en-us/sharepoint/collaboration)

### Continuous Integration
- [Jenkins](https://jenkins.io/)
  - [Jenkins X](jenkins_x/)
- [Travis CI](https://travis-ci.org/)
- [Atlassian Bamboo](bamboo/)
- [CircleCI](https://circleci.com/)
- [Microsoft VSTS](https://visualstudio.microsoft.com/team-services/)
- [Microsoft TFS](https://visualstudio.microsoft.com/tfs/)
- [CodeFresh](codefresh/)
- TeamCity
- Codeship
- Semaphore
- AppVeyor
- Urban code
- Hudson
- cruisecontrol
- [Electric Cloud](http://electric-cloud.com/products/electricaccelerator/)
- Jet Brains
- [Zuul](https://zuul-ci.org/)
- [CloudBuild](cloud-build/)

### Container Registry
- [JFrog Artifactory](https://jfrog.com/artifactory/)
- [Docker Hub](https://hub.docker.com/)
- [Docker Trusted Registry](https://docs.docker.com/ee/dtr/)
- [RedHat/CoreOS Quay](https://coreos.com/quay-enterprise/)
- [Amazon ECR](https://aws.amazon.com/ecr/)
- [Azure ACR](https://azure.microsoft.com/en-us/services/container-registry/)
- [Google Cloud Container Registry](https://cloud.google.com/container-registry/)

### Continuous Delivery/Deployment
- [Jenkins](https://jenkins.io/)
  - [Jenkins X](jenkins_x/)
- [Spinnaker](https://www.spinnaker.io/)
- [Go CD](https://www.gocd.org/index.html)
- [Puppet Pipelines](https://puppet.com/products/puppet-pipelines)
- [Cloudbees Codeship](https://codeship.com/)
- [Electric Cloud ElectricFlow](http://electric-cloud.com/products/electricflow/)
- [CA Technologies](https://www.ca.com/us.html)
- [XebiaLabs](https://xebialabs.com/)
- [Microsoft VSTS](https://visualstudio.microsoft.com/team-services/)
- [Microsoft TFS](https://visualstudio.microsoft.com/tfs/)
- [CodeFresh](codefresh/)
- Red Hat Ansible
- Clarive
- Micro Focus
- Octopus Deploy
- OpenMake Software
- [IBM UrbanCode](https://developer.ibm.com/urbancode/products/urbancode-deploy/)
- [AWS CodeDeploy](https://aws.amazon.com/codedeploy/)
- [CenturyLink Cloud Application Manager](https://www.ctl.io/cloud-application-manager/)
- Inedo
- Arcad Software
- [HashiCorp Nomad](https://www.nomadproject.io/)
- [CloudBuild](cloud-build/)

### Infrastructure Configuration
- Puppet
- Chef
- Ansible
- SaltStack
- [HashiCorp Terraform](https://www.terraform.io/)
- [HashiCorp Packer](https://www.packer.io/)
- Docker
- Microsoft PowerShell
- CFEngine
- Apache Maven
- AWS OpsWorks
- SmartFrog
- [Jenkins X](jenkins_x/)

### Application Performance Monitoring
- [New Relic](https://newrelic.com/application-monitoring/features)
- appdynamics
- [Splunk](https://www.splunk.com/)
- [Zabbix](https://www.zabbix.com/)
- [Nagios](https://www.nagios.org/)
- [Solarwinds](https://www.solarwinds.com/log-event-manager-software)
- Ganglia
- Monit
- Scalyr
- Graylog
- UpGuard
- Sensu
- SysDig
- [sumologic](https://www.sumologic.com/)
- [logic monitor](https://www.logicmonitor.com/)
- Sentry
- Rollbar
- Airbrake

### Security Testing
- [CA Veracode](https://www.veracode.com/)
- SonarQube
- HPE Fortify
- Checkmarx
- Black Duck
- Synopsys
- Acunetix
- Contrast Security
- ERPScan
- Fasoo
- Nexus LifeCycle
- NSFOCUS
- N-Stalker
- PortSwigger
- Qualys
- Rapid7
- SiteLock
- Snyk
- Trustwave
- Virtual Forge
- WhiteHat Security
- WhiteSource

### License Management
- Black Duck
- WhiteSource
- ManageEngine
- Flexera
- Snow Software

## BizOps Tools
[Meltano](https://gitlab.com/meltano/meltano) is a separate product that isn't part of GitLab. Here's a list of BizOps tools. Other lists include [cloud native ETL tools](https://www.alooma.com/blog/etl-tools-modern-list), and the [Meltano README](https://gitlab.com/meltano/meltano#data-science-lifecycle).  
- [AWS Glue](https://aws.amazon.com/glue/)
- [Alooma](https://www.alooma.com/blog/etl-tools-modern-list)
- [Looker](https://looker.com/)
- Pentaho
- Tableau
- [Periscope](https://www.periscopedata.com/)
- Qlik
- Microsoft
- SiSense
- ThoughtSpot
- MicroStrategy
- Birst
- Domo
- SAP
