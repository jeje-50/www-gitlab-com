---
layout: markdown_page
title: "Analyst Relations"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Analyst product categorizations

GitLab is a single application that spans many product categories.
Forrester and Gartner define several of these categories and their definition
of a space can be useful to determine what important competing products are.
Below, some relevant categories are listed that GitLab is or will be competing in.

### Forrester: Continuous Integration Tools

- Leaders: GitLab CI, CloudBees Jenkins, CircleCI, Microsoft
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Continuous+Integration+Tools+Q3+2017/-/E-RES137261)

### Forrester: Configuration Management Software For Infrastructure Automation

- Leaders: Puppet Enterprise, Chef Automate
- [Link to report](https://reprints.forrester.com/#/assets/2/675/'RES137964'/reports)

### Gartner: Software Change and Configuration Management Software

- Market Guide: Amazon Web Services, Atlassian, BitKeeper, CA Technologies, Codice Software, Collabnet, GitHub, GitLab, IBM, Micro Focus/Borland, Microsoft, Perforce, PTC, SeaPine Software, Serena, SourceGear, Visible Systems, WANdisco, Wildbit
- [Link to report](https://www.gartner.com/document/3118917)
- Market Guide update initiated March 2018.

### Gartner: Continuous Configuration Automation Tools

- Market Guide: Chef, CFEngine, Inedo, Orca, Puppet, Red Hat, SaltStack
- [Link to report](https://www.gartner.com/document/3843365)

### Gartner: Software Test Automation

- Leaders: MicroFocus, Tricentis
- [Link to 2016-11-15 report](https://www.gartner.com/document/3512920)
- [Link to 2017-11-20 report](https://www.gartner.com/document/3830082)

### Forrester: Modern Application Functional Test Automation Tools

- Leaders: Parasoft, IBM, Tricentis, HPE
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Modern+Application+Functional+Test+Automation+Tools+Q4+2016/-/E-RES123866)

### Gartner: Performance Testing

- Market Guide: Automation Anywhere, BlazeMeter, Borland, CA Technologies, HPE, IBM, Neotys, Oracle, Parasoft, RadView, SmartBear, Soasta, Telerik, TestPlant
- [Link to report](https://www.gartner.com/document/3133717)

### Gartner: Application Release Automation

- Leaders: Electric Cloud, CA (Automic), XebiaLabs, IBM
- [Magic quadrant](http://electric-cloud.com/resources/whitepapers/gartner-magic-quadrant-application-release-automation/)
- MQ update underway. Research kicked off March 2018


### Forrester: Continuous Delivery and Release Automation

- Leaders: Chef, CA, Microsoft
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Continuous+Delivery+And+Release+Automation+Q3+2017/-/E-RES137969)

### Gartner: Application Performance Monitoring Suites

- Leaders: New Relic
- [Link to report](https://www.gartner.com/doc/3551918)

### Gartner: Application Security Testing

- Leaders: Micro Focus, CA Technologies (Veracode), Checkmarx, Synopsys, IBM
- [Link to 2017-02-28 report](https://www.gartner.com/doc/3623017)
- [link to 2018-03-19 report](https://www.gartner.com/doc/3868966/magic-quadrant-application-security-testing)

### Forrester: Application Security Testing

- Leaders: Synopsys, CA Veracode
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Static+Application+Security+Testing+Q4+2017/-/E-RES139431)

### Gartner: Project Portfolio Management

- Leaders: Planview, CA Technologies, Changepoint
- [Link to report](https://www.gartner.com/document/3728917)

### Forrester: Strategic Portfolio Management Tools

- Leaders: AgileCraft, CA, ServiceNow
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Strategic+Portfolio+Management+Tools+Q3+2017/-/E-RES136707)

### Forrester: Portfolio Management For The Tech Management Agenda

- Leaders: CA, Planview
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Portfolio+Management+For+The+Tech+Management+Agenda+Q1+2015/-/E-RES114742)

### Gartner: Enterprise Agile Planning Tools

- Leaders: CA, Atlassian, VersionOne
- [Link to report](https://www.gartner.com/doc/3695417)
- Research for next MQ commences Summer 2018

### Forrester: Enterprise Collaborative Work Management

- Leaders: Clarizen, Redbooth, Wrike, Planview, Asana, and Smartsheet
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Enterprise+Collaborative+Work+Management+Q4+2016/-/E-RES121721)

### Forrester: Application Life-Cycle Management, Q4 2012

- Covered in report: Atlassian, CollabNet, HP, IBM, Microsoft, PTC, Rally Software, Rocket Aldon, and Serena Software
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Application+LifeCycle+Management+Q4+2012/-/E-RES60080)

### Gartner: Container Management Software

- Market Guide: Apcera, Apprenda, CoreOS, Docker, Joyent, Mesosphere, Pivotal, Rancher Labs, Red Hat
- [Link to report](https://www.gartner.com/document/3782167)

### Gartner: Enterprise Application Platform as a Service

- Leaders: Salesforce, Microsoft
- [Link to report](https://www.gartner.com/document/3263917)

### Gartner: Data Science Platforms

- Leaders: IBM, SAS, RapidMiner, KNIME
- [Link to report](https://www.gartner.com/doc/3606026)