---
layout: markdown_page
title: "Summit Training Leadership Perspective"
---

## On this page
{:.no_toc}

- TOC
{:toc}


<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/39chczWRKws?start=1756" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

For the context of this video please see the [summit challenges in the Greece recap](https://about.gitlab.com/2017/10/25/gitlab-summit-greece-recap/#summit-challenges).

## The Summit

Please see the [goal of the summit](https://about.gitlab.com/culture/summits/#goal).

**What it is**:

- Company meet up
- Differentiator & investment in GitLabbers
- Meet fellow GitLabbers across all departments & regions
- Part team building, part education, part customer interaction - hopefully all fun!

**What it is not**:

- Mandatory Trip
- Vacation/Holiday, incentive trip
- Department offsite

References:
[Summit Page](https://about.gitlab.com/culture/summits/)
[Summit Project](https://gitlab.com/summits/2018-Summit/blob/master/urgent-important-info.md)


## Responsibilities as a Leader

- [GitLab values](https://about.gitlab.com/handbook/values/) - model behaviors and values at all times
  - [Permission to play](https://about.gitlab.com/handbook/values/#permission-to-play)
- Engage with team members across the organization
- Watch for team dynamics; Be mindful of silos and favoritism
- Serve as a “host”


## Be Conscious

**Potential issues:**
- Sexual / harassment
- Excessive drinking / drug use
- Fighting / physical altercations
- Accidents (trips, falls, motor accidents, etc.)
- Inappropriate dress (business/work event)
- Various mental state/moods (anxiety, depression, etc); [neurodiversity](https://about.gitlab.com/handbook/values/#diversity)
- Cultural impacts
  - We have various generations & 40+ countries/cultures coming together for 5 straight days, versus our normal routine of interacting remotely.
  - We are guests of this country, of the hotel, of the sites we will visit - it is our privilege to be there & we need to be ambassadors of GitLab and lead by example the whole time according to [our values](https://about.gitlab.com/handbook/values/).
- Be sensitive to political, religious and other potentially divisive conversations

**What should a manager do?**
- Protect and reduce risk to all team members and to the company
- Check on your team throughout summit
- Err on the side of caution and over-communication by contacting People Ops immediately if there is an issue or if you think there may be a problem/concern

**Who to contact?**
- Barbie Brewer and Julie Armendariz - onsite at Summit in South Africa
- Jessica Mitchell - available via Slack
- People Operations Team - can be reached via Slack and people ops email address

## Manager Stories

On the live training (the video of which will be added here) we asked three managers to share their insights. The questions asked were as follows:

1. As a manager, what surprised you most about attending summit?
2. What was 1 of the biggest challenges you face as a leader at a previous summit?
3. What advice do you have for managers, especially new managers attending for the first time?

## Extraverts and Introverts

**Extraverts**

- Recharge by being Social
- Enjoy group conversations
- Speak more
- Make decisions quickly
- Love getting attention
- Speak up in meetings

**Introverts**

- Recharge by spending time alone
- Enjoy one-on-one conversations
- Listen more
- Reflect before making decisions
- Are not interested in getting attention
- Shares ideas when prompted

**Ambiverts**

- A combination of both introverts and extraverts


References: 
[Are Extraverts Happier Than Introverts? Psychology Today](https://www.psychologytoday.com/blog/thrive/201205/are-extroverts-happier-introverts)
[Are You an Extravert, Introvert, or Ambivert?](https://www.psychologytoday.com/us/blog/cutting-edge-leadership/201711/are-you-extravert-introvert-or-ambivert)

## Cultural Differences
It is important to recognize that people from different cultures have different ways of saying things, different body language, different ways of dressing/attire and even different ways of looking at things.  You can review examples of typical cultural differences on the [Center of Intercultural Competence](http://www.cicb.net/en/home/examples)



## Lost in Translation
Translation tends to sound easier than it is.  People often think that it is just a matter of replacing each source word with a corresponding translated word, and then you are done.  Assume best intent from your fellow team members and use it as an opportunity start a new dialogue.  For more information you can review the following articles on [LTC Language Solutions](https://ltclanguagesolutions.com/blog/lost-in-translation-translating-cultural-context/)

![Translation](/images/training/Translation.png)
