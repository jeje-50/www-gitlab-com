(function() {
  var competitorsContainers = document.getElementsByClassName('competitors-container');

  function getLogos() {
    for (var i = 0; i < competitorsContainers.length; i++) {
      var tlLeft = new TimelineMax({ repeat: -1 });
      tlLeft.staggerTo(competitorsContainers[i].children, 1, {ease: Power4.easeOut, opacity: 1}, 4);
      tlLeft.staggerTo(competitorsContainers[i].children, 1, {ease: Power4.easeOut, opacity: 0}, 4);
      tlLeft.play();
    }
  }

  window.addEventListener('load', getLogos);
})();
