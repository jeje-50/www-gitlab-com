---
layout: markdown_page
title: "Developer platform"
---

## Introduction

Developers ecosystem (building on your product)
For example a game development platform, investment management platform, or a customer success platform.

## Benefits

1. Make it easier to get started, increased conversion.
1. Sticky, developers less likely to leave, less churn.
1. Able to communicate new features, expand usage of new features.
1. Listen better, increase product market fit.

## Templates


## Open issue tracker


## Innersourcing



## PaaS



## Security


